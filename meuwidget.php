<?php
/*
Plugin Name: MeuWidget
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Plugin Erik Vídeos
Version: 1.0
Author: Erik Comelli
Author URI: http://erikcomelli.hostoi.com
*/
?>

<?php
class meuwidget extends WP_Widget{
	
	//Registrar o widget Wordpress
	function meuwidget(){
		$widget_ops = array(
			'classname' => 'minhaclassecss',
			'description' => 'Widget para vídeos'
		);
		$this->WP_Widget('meuwidget','Plugin Erik Vídeos',$widget_ops);
		
		//verifica se o widget está ativo e chama a função que cria o css
		if ( is_active_widget( false, false, $this->id_base ) ) {
			add_action('wp_head', array($this,'css'));
		}		
	}
	
	//css
	function css(){
		?>
		<style type="text/css">
		
		.widget-box{
			background:#f0f0f0;
		}	
		.widget-titulo{
			text-align:center;
		}	
		
		</style>
		<?php
	}	
	
	//back-end
 	public function form( $instance ) {
 		//seta os valores padrões
 		$defaults = array(
 			'titulo' => 'Título',
 			'tamanhoFonte' => 20,
 			'corBox' => 'f0f0f0',
 			'url' => 'url aqui'
 		);
 		//faz um merge dos valores padrões com os valores informados pelo usuário
 		$instance = wp_parse_args((array) $instance,$defaults);
 		$titulo = $instance['titulo'];
		//$fonte = (int)$instance['tamanhoFonte'];
		$tamanhoFonte = $instance['tamanhoFonte'];
 		//var_dump($fonte);
 		//echo ('form');die();			
 		
 		$corBox = $instance['corBox'];
 		$url = $instance['url'];
 		
 		?>

 		<p> Título do Vídeo: 
 			<input class="widefat" name="<?php echo $this->get_field_name('titulo');?>" type="text" value="<?php echo esc_attr($titulo);?>"/>
 		</p>
 		<p>
 			Tamanho da fonte do título:
 			<br/>
 			<span class="description"> Informe um valor entre 8 e 50.</span>
 			<input class="widefat" name="<?php echo $this->get_field_name('tamanhoFonte');?>" type="text" value="<?php echo esc_attr($tamanhoFonte);?>"/>
 		</P>
 		<p>
 			Cor do Box:
 			<br/>
 			<span class="description"> Informe um valor hexadecimal.</span>
 			<input class="widefat" name="<?php echo $this->get_field_name('corBox');?>" type="text" value="<?php echo esc_attr($corBox);?>"/> 
 		</p>
 		<p> Url do vídeo: 
 			<br/>
 			<span class="description"> iframe embed do youtube ou vimeo. </span>
 			<input class="widefat" name="<?php echo $this->get_field_name('url');?>" type="text" value="<?php echo esc_attr($url);?>"/>
 		</p>	
 		<?php 	  
	}

	//salva
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		//verifica se o tamanho da fonte informado é válido
		$fonte = (int)$new_instance['tamanhoFonte'];
		
		//verifica se o tamanho informado é válido. Se o mesmo for inválido, retorna o valor anterior.
 		if(!(($fonte>8) && ($fonte <=50))){
 			$fonte = $instance['tamanhoFonte'];
 		} 		
		
		$instance['titulo'] = sanitize_text_field($new_instance['titulo']);
		$instance['tamanhoFonte'] = $fonte;
		$instance['corBox'] = sanitize_text_field($new_instance['corBox']);
		//$instance['url'] = sanitize_text_field($new_instance['url']);
		$instance['url'] = $new_instance['url'];
		return $instance;
	}
	
	//front-end
	public function widget( $args, $instance ) {
		//transforma o array associativo em variáveis
		extract($args);		
		echo $before_widget;		
		$titulo = empty($instance['titulo'])?'&nbsp' : $instance['titulo'];
		$tamanhoFonte = $instance['tamanhoFonte'];
		$corBox = $instance['corBox'];
		$url = empty($instance['url'])?'&nbsp' : $instance['url'];		
		echo '<div class="widget-box" style="background:#'.$corBox.'">';		
		
		if (!empty($titulo)){	
			echo '<div class="widget-titulo">';		
			echo '<p style="font-size:'.$tamanhoFonte.'px">'.
			esc_html($titulo).
			'</p>';
			echo ('</div>');
		}
		
		echo '<div>';
		echo $url;		
		echo '</div>';		
		echo '<center><script type="text/javascript" src="https://www.klickmail.com.br/form.php?id=3537&type=js"></script><noscript><iframe class="ktiframe" src="https://www.klickmail.com.br/form.php?id=3537&type=if" frameborder="0" allowtransparency="true" width="286px" height="216px" scrolling="no"></iframe></noscript></center>';
		echo '</div>';		
		echo $after_widget;
	}	
}

function meuwidget_register_widgets() {
	register_widget( MeuWidget);
}

add_action( 'widgets_init', meuwidget_register_widgets);

?>